
function(embed_file file source)
    add_custom_command(
        OUTPUT ${file}
        COMMAND xxd -i ${source} > ${CMAKE_CURRENT_BINARY_DIR}/${file}
        WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
        MAIN_DEPENDENCY ${source})
endfunction()

