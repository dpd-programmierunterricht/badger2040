#include "util/bmp.h"
#include "util/screen.h"

#include <stdio.h>

#include <algorithm>
#include <cstring>
#include <string>

#include "badger2040.hpp"
#include "common/pimoroni_common.hpp"
#include "pico/platform.h"
#include "pico/stdlib.h"
#include "pico/time.h"

using namespace pimoroni;

Badger2040 badger;

extern unsigned char image_bmp[];
extern unsigned char avatar_bmp[];

void draw_default() {
  badger.pen(15);
  badger.clear();

  badger.font("sans");

  // draw top bar with logo
  badger.pen(0);
  badger.rectangle(0, 0, 168, 40);
  draw_bmp(badger, image_bmp, 5, 0, 1);

  // draw avatar on side
  draw_bmp(badger, avatar_bmp, 168, 0, 0);

  badger.pen(0);
  badger.thickness(4);
  badger.text("PATRICK", 2, 62, 1.0f);
  badger.text("ELSEN", 2, 94, 1.0f);
  badger.thickness(2);
  badger.text("C++ TEACHER", 2, 120, 0.6f);

  badger.update();
}

void draw_hello() {
  badger.pen(15);
  badger.clear();

  badger.font("sans");

  badger.pen(0);
  badger.rectangle(0, 0, 296, 45);
  badger.pen(15);
  badger.text("HELLO!", 115, 14, 0.6f);
  badger.text("MY NAME IS", 80, 35, 0.7f);

  badger.pen(0);
  badger.thickness(4);
  badger.text("PATRICK", 80, 62, 1.0f);
  badger.text("ELSEN", 100, 94, 1.0f);
  badger.thickness(2);
  badger.text("C++ TEACHER", 80, 120, 0.6f);

  badger.update();
}

enum screen {
    SCREEN_DEFAULT,
    SCREEN_HELLO,

    // Used to get count of screens
    SCREEN_MAX,
};

screen_handler handlers[] = {
    SCREEN_HANDLER(SCREEN_DEFAULT, draw_default),
    SCREEN_HANDLER(SCREEN_HELLO, draw_hello),
    SCREEN_HANDLER_END(SCREEN_MAX),
};

int main() {
  stdio_init_all();

  sleep_ms(500);

  badger.init();
  badger.update_speed(2);

  screen current_screen = SCREEN_DEFAULT;

  while (true) {
    draw_screen(handlers, current_screen);

    badger.wait_for_press();

    if (badger.pressed(badger.DOWN)) {
        current_screen = (screen) screen_next(SCREEN_MAX, current_screen);
    }

    if (badger.pressed(badger.UP)) {
        current_screen = (screen) screen_prev(SCREEN_MAX, current_screen);
    }
  }
}
