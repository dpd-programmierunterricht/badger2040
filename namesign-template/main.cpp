#include "util/bmp.h"

#include <stdio.h>

#include <algorithm>
#include <cstring>
#include <string>

#include "badger2040.hpp"
#include "common/pimoroni_common.hpp"
#include "pico/platform.h"
#include "pico/stdlib.h"
#include "pico/time.h"

using namespace pimoroni;

Badger2040 badger;

extern unsigned char image_bmp[];

void draw() {
  badger.pen(15);
  badger.clear();

  badger.font("sans");

  badger.pen(0);
  badger.rectangle(0, 0, 296, 45);
  badger.pen(15);
  badger.text("HELLO!", 115, 14, 0.6f);
  badger.text("MY NAME IS", 80, 35, 0.7f);

  //draw_bmp(badger, image_bmp, 0, 0, 1);

  badger.pen(0);
  badger.thickness(4);
  badger.text("DEIN", 80, 62, 1.0f);
  badger.text("NAME", 100, 94, 1.0f);
  badger.thickness(2);
  badger.text("C++ TEACHER", 80, 120, 0.6f);

  badger.update();
}

int main() {
  stdio_init_all();

  sleep_ms(500);

  badger.init();
  badger.update_speed(2);

  while (true) {
    draw();

    badger.wait_for_press();
  }
}
