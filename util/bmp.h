#include <stdio.h>
#include <cstring>
#include "badger2040.hpp"

void draw_bmp(pimoroni::Badger2040 &badger, unsigned char *data, int offset_x, int offset_y, int invert) {
    if(data[0] != 'B' && data[1] != 'M') {
        return;
    }

    uint32_t length;
    memcpy(&length, &data[2], 4);

    uint32_t pixels;
    memcpy(&pixels, &data[10], 4);

    uint32_t width;
    uint32_t height;
    memcpy(&width, &data[18], 4);
    memcpy(&height, &data[22], 4);

    uint32_t stride = ((width + 31) / 32) * 4;

    for(int y = 0; y < height; y++) {
        int pos = y * stride;

        for(int x = 0; x < width; x++) {
            if(data[pixels + y * stride + (x / 8)] & (128 >> (x % 8))) {
                badger.pen(invert ? 0 : 15);
            } else {
                badger.pen(invert ? 15 : 0);
            }
            badger.pixel(offset_x + x, offset_y + height - y - 1);
        }
    }
}

