
int screen_next(int max, int cur) {
    return ((cur + 1) % max);
}

int screen_prev(int max, int cur) {
    return ((cur + max - 1) % max);
}

struct screen_handler {
    int screen;
    void (*handler)();
};

void draw_screen(screen_handler handlers[], int screen) {
    for(int i = 0; handlers[i].handler != nullptr; i++) {
        if(handlers[i].screen == screen) {
            handlers[i].handler();
        }
    }
}

#define SCREEN_HANDLER(_screen, _handler) { .screen = _screen, .handler = _handler }
#define SCREEN_HANDLER_END(_max) { .screen = _max, .handler = nullptr }
